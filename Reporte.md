# Reporte de usuarios en git

## 1. Guest

Tendiendo los permisos de invitado en un proyecto ajeno, permite:

* Crear y modificar archivos dentro de la rama master en la sección *working directory*.
* Crear commits, es decir, pasar las modificaciones al *staging area*.
* Crear issues.
* Cambiarse de una rama a otra.

Por otro lado, no permite:

* Realizar push al proyecto, en otras palabras, guardar las modificaciones realizadas en el repositorio respectivo 
* Crear nuevas ramas.
* Crear *merge request* a otra rama.
* Eliminar una rama con el comando $git branch -d *[branch_name]*.
* Eliminar una rama de manera forzada con el comando $git branch -D *[branch_name]*.
* Eliminar issues.
  
## 2. Reporter

Teniendo los permisos de reporter en un proyecto, permite realizar las acciones descritas anteriormente como guest, y además:

* Permite eliminar otras ramas con el comando $git branch -d *[branch_name]*. 
* Permite eliminar de manera forzada otras ramas con el comando $git branch -D *[branch_name]*.

Sin embargo, las limitaciones respecto al push, al *merge request* y a los issues se mantienen, es decir, no permite:

* Realizar push de los archivos modificados hacia el repositorio.
* Crear *merge request* a otra rama.
* Eliminar issues.

## 3. Developer

Con los permisos de developer en un proyecto ajeno, se pueden realizar todas las acciones permitidas tanto como guest y como reporter. Además, teniendo los permisos como developer se puede:

* Generar *merge request* a otras ramas.
* Generar *merge request* a la rama master, sin embargo, el creador del proyecto debe ser quien haga el merge. En otras palabras, el merge no puede realizarse de manera directa por el usuario developer que lo solicite.
* Realizar push al repositorio en el cual se aloja el proyecto.

A persar de que con permiso de developer aumentan los privilegios y las actividades que puede realizar un usuario con dichos permisos, se mantiene el conflicto con los issues:

* No se pueden eliminar issues.

## 4. Mantainer

Como mantainer, se pueden realizar las acciones descritas anteriormente lara los permisos como guest, reporter y developer, además, como mantainer se puede:

* Generar un *merge request* a la rama master y sin necesidad de esperar la aprovación del ower, realizar el merge.

A nivel de mantainer, los permisos son prácticamente parecidos a los que se tiene como owner, sin embargo, el conflicto con los issues se mantiene: 

* No se pueden eliminar issues.