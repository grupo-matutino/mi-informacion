Nombre: Andrés Torres Albuja
Edad: 22 años
Semestre proximo a cursar: 6to semestre 

Hace apenas un día terminó el semestre, y lo superé con buenas notas, en estos momentos me encuentro en el norte de la ciudad, específicamente en las capacitaciones de Manticore. La experiencia hasta ahora ha sido bastante llevadera, sin novedades, sólo temo no llegar a aprender lo suficiente en tan corto tiempo. Si por alguna razón llego a desertar o algo me pasara, no pido ningún tipo de represalias. Mi nombre es Andrés Torres y esta es mi historia.